function renderRow(user) {
    return `
        <tr>
            <td>${user.name}</td>
            <td>${user.descr}</td>
            <td>${user.yearOfBird}</td>
            <td>${user.city}</td>
            <td>${user.result ? 'так' : 'ні'}</td>
        </tr>`;
}

function render(results) {
    let tableContent = '';
    for (let i = 0; i < results.length; i++) {
        tableContent = tableContent + renderRow(results[i]);
    }

    document.getElementById('users-table').innerHTML = tableContent;
}