const pensionAge = 60;
const childAge = 5;
const currentYear = new Date().getFullYear();
const pensionFreeTimeRange = {
    from: (new Date('01/01/2019 09:00')).getTime(),
    to: (new Date('01/01/2019 11:00')).getTime(),
};
const currentTime = (new Date()).getTime();
//const currentTime = (new Date('01/01/2019 10:00')).getTime();
//const currentDayOfWeek = (new Date()).getDay();
const currentDayOfWeek = 6; // неділя

const users = [{
    name: 'Зеновій',
    descr: 'пенсіонер',
    yearOfBird: 1957,
    city: 'Івано-Франківськ'
}, {
    name: 'Василь',
    descr: 'пенсіонер',
    yearOfBird: 1959,
    city: 'Київ'
}, {
    name: 'Микола',
    descr: 'пенсіонер',
    yearOfBird: 1954,
    city: 'Львів'
}, {
    name: 'Оленка',
    descr: 'дитина',
    yearOfBird: 2017,
    city: 'Київ'
}, {
    name: 'Петро',
    descr: 'інженер',
    yearOfBird: 1979,
    city: 'Київ'
}, {
    name: 'Станіслав',
    descr: 'студент',
    yearOfBird: 2000,
    city: 'Івано-Франківськ'
}];

class SpecPensioner extends Specification {
    isSatisfied (user) {
        return (currentYear - user.yearOfBird) >= pensionAge;
    }
}

class SpecChild extends Specification {
    isSatisfied (user) {
        return (currentYear - user.yearOfBird) <= childAge;
    }
}

class SpecPensionFreeTimeRange extends Specification {
    isSatisfied () {
        return (currentTime >= pensionFreeTimeRange.from) && (currentTime <= pensionFreeTimeRange.to);
    }
}

class SpecWeekend extends Specification {
    isSatisfied () {
        return (currentDayOfWeek >= 5) && (currentDayOfWeek <=6);
    }
}

class SpecCitizen extends Specification {
    isSatisfied (user) {
        return user.city === 'Івано-Франківськ';
    }
}

const isChildHasFreePassage = new SpecChild();
const isPensionerHasFreePassage = new SpecPensioner().and(new SpecPensionFreeTimeRange().or(new SpecCitizen()));
const isCitizenHasFreePassage = new SpecCitizen().and(new SpecWeekend());

const isUserHasFreePassage = isChildHasFreePassage.or(isPensionerHasFreePassage).or(isCitizenHasFreePassage);

const results = [];

users.map(function(user) {
    results.push(user);
    results[results.length - 1].result = isUserHasFreePassage.isSatisfied(user);
});

render(results);
